import React from 'react';
import Nav from './Nav';

function App(props) {
  return (
    <>
      <Nav />
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {props.attendees && props.attendees.map(attendee => (
              <tr key={attendee.href}>
                <td>{attendee.name || "N/A"}</td>
                <td>{attendee.conference || "N/A"}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default App;